<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\User;
use App\Role;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/create',function(){
    $user= User::findOrFail(1);

    $role = new Role(['name'=>'subs']);

    $user->roles()->save($role);
});

Route::get('/read',function(){
    $user=User::findOrFail(1);
    // dd($user);
    foreach($user->roles as $rule){
        // echo $rule->name."</br>";
        // dd($rule);
    }
    // return $user->roles;
});

Route::get('/update', function(){
    $user = User::findOrFail(1);

    if($user->has('roles')){
        foreach($user->roles as $role){
            if($role->name == 'admin'){
                $role->name = 'administrator';
                $role->save();
            }
        }
    }
});

//delete user dengan id 1 dan role dengan id 1
Route::get('/delete', function(){
    $user =User::findOrFail(1);
    foreach($user->roles as $role){
        $role->whereId(1)->delete();
    }
});

Route::get('/attach', function(){
    $user=User::findOrFail(1);
    $user->roles()->attach(4);
});


Route::get('/detach', function(){
    $user=User::findOrFail(1);
    $user->roles()->detach();
});

Route::get('/sync',function(){
    $user=User::findOrFail(1);
    $user->roles()->sync([2,3,4]);
});